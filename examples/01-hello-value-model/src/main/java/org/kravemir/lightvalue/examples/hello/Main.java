package org.kravemir.lightvalue.examples.hello;

import java.io.PrintStream;

public class Main {

    private static final String SEPARATOR = "============================================================";

    private static void sendMessage(GreetingsMessage message, PrintStream output) {
        output.println(SEPARATOR);
        output.println("From: " + formatContactAddress(message.getFrom()));
        output.println("To:   " + formatContactAddress(message.getTo()));
        output.println(SEPARATOR);

        output.println(String.format("Hello %s!", message.getTo().getName()));
        output.println();
        output.println("Best wishes,");
        output.println(message.getFrom().getName());
        output.println(SEPARATOR);
    }

    private static String formatContactAddress(Contact contact) {
        return String.format("%s <%s>", contact.getName(), contact.getEmail());
    }

    public static void main(String[] argv) {
        GreetingsMessage message = GreetingsMessage.newBuilder()
                .setFrom(Contact.newBuilder()
                        .setName("LightValue Example01")
                        .setEmail("something@somewhere.in.space")
                        .build()
                )
                .setTo(Contact.newBuilder()
                        .setName("LightValue User")
                        .setEmail("someone@earth.planet")
                        .build()
                )
                .build();

        sendMessage(message, System.out);
    }
}
