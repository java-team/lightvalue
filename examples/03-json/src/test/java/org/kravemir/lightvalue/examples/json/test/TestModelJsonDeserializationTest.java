package org.kravemir.lightvalue.examples.json.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.kravemir.lightvalue.examples.json.JacksonMixIns;
import org.kravemir.lightvalue.examples.json.TestModel;

import java.io.IOException;
import java.net.URL;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestModelJsonDeserializationTest {

    private ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        objectMapper = new ObjectMapper();
        JacksonMixIns.registerMixIns(objectMapper);
    }

    @Test
    public void testDeserialization() throws IOException {
        // given
        URL resource = getClass().getResource("/test.json");

        // when
        TestModel value = objectMapper.readValue(
                resource,
                TestModel.class
        );

        // them
        assertThat( value, is(
                TestModel.newBuilder()
                        .setB(true)
                        .setI(42)
                        .setD(3.14)
                        .setS("A String")
                        .setInner1(
                                TestModel.InnerTest.newBuilder()
                                        .setA("string, a 1")
                                        .build()
                        )
                        .setInner2(
                                TestModel.InnerTest.newBuilder()
                                        .setA("string, a 2")
                                        .build()
                        )
                        .setInner3(
                                TestModel.AnotherInner.newBuilder()
                                        .setB("string, b 3")
                                        .build()
                        )
                        .build()
        ));
    }
}
