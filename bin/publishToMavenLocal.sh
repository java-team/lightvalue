#!/usr/bin/env bash

./gradlew wrapper --gradle-version 4.10.2

./gradlew --console=plain \
    -PenablePublishing \
    clean publishToMavenLocal