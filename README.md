LightValue
==========

LightValue is made to generate models for libraries as value classes, which are also JSON serialization friendly.

The primary goal is:

* **NO mandatory runtime DEPENDENCIES** for generated value classes. 

Usage of Gradle Plugin
-----

### Include in gradle project

```groovy
buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath 'org.kravemir.lightvalue:lightvalue-gradle-plugin:0.8.1'
    }
}

apply plugin: 'java'
apply plugin: 'org.kravemir.lightvalue'
```

### Default convention

Default convention creates a lightvalue configuration for each java sourceset, if Java plugin as applied.

Read more about default convention approach in [Convention over configuration](https://guides.gradle.org/designing-gradle-plugins/#convention_over_configuration) guide.

See [examples](#examples), as they utilize default convention.

### Custom convention

To use own custom conventions, apply only `LightValueBasePlugin`, or create custom tasks of type `LightValueGeneratorTask`.

### Configuration

Each configuration is of `LightValueConfiguration` java type, and has following properties and methods:


| Name                       | Description                                                  |
| -------------------------- | ------------------------------------------------------------ |
| **`name`** *(read-only)*   | Name of configuration                                        |
| **`source`**               | method adds  `path` (single parameter) to list of sources    |
| **`outputDir`**            | property defines output directory for generation of value classes |
| **`registerInSourceSets`** | method registers generated output to `sourceSets`(varargs parameter), and adds dependency to each source set compile java task |

### Examples

| Name                                                    | Description                                                  |
| ------------------------------------------------------- | ------------------------------------------------------------ |
| [01. Hello Value Models](examples/01-hello-value-model) | Just, a Hello World for LightValue models using gradle       |
| [02. Data Types](examples/02-data-types)                | [TODO] will contain supported data types                     |
| [03. JSON](examples/03-json)                            | Usage optional integration with Jackson                      |
| [04. multi project](examples/04-multiproject)           | [TODO] will contain example how to use in multi (sub)projects and depedencies |

Usage of Java library
---------------------

TODO: not yet documented

Quality Assurance
-----------------

The quality assurance is based on [Testing Gradle plugins](https://guides.gradle.org/testing-gradle-plugins/) guide:

* manual tests based on [examples](examples), and check that:
    * end-user API remains unchanged across versions,
    * generated stuff works as expected,
* unit tests in [generator's tests](lightvalue-generator/src/test),
* and, nothing else now,...

License
-------

The whole project licensed is under Apache-2.0 license. 