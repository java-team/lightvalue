package org.kravemir.lightvalue;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import java.util.HashMap;
import java.util.Map;

public class ModelGenerationContext {
    private String packageName;
    private Map<String, TypeName> classes = new HashMap<>();

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public TypeName getType(String typeRawName) {
        switch (typeRawName) {
            case "bool":
                return TypeName.BOOLEAN;
            case "int32":
                return TypeName.INT;
            case "double":
                return TypeName.DOUBLE;
            case "string":
                return ClassName.get(String.class);
        }

        return classes.get(typeRawName);
    }

    public void registerType(String name, TypeName subModelType) {
        if(classes.get(name) != null) {
            throw new RuntimeException(String.format("Type %s is already registered in: %s", name, nameThisContext()));
        }

        classes.put(name, subModelType);
    }

    protected String nameThisContext() {
        return "root context";
    }
}
