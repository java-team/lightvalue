package org.kravemir.lightvalue.model;

public class PropertyDef {

    private final String name;
    private final String type1;
    private final String type2;
    private final boolean repeated;
    private final boolean map;

    public PropertyDef(String name, String type1, String type2, boolean repeated, boolean map) {
        this.name = name;
        this.type1 = type1;
        this.type2 = type2;
        this.repeated = repeated;
        this.map = map;
    }

    public String getName() {
        return name;
    }

    public String getType1() {
        return type1;
    }

    public String getType2() {
        return type2;
    }

    public boolean isRepeated() {
        return repeated;
    }

    public boolean isMap() {
        return map;
    }

    @Override
    public String toString() {
        return "PropertyDef{" +
                "name='" + name + '\'' +
                ", type1='" + type1 + '\'' +
                ", type2='" + type2 + '\'' +
                ", repeated=" + repeated +
                ", map=" + map +
                '}';
    }
}
