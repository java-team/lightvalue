package org.kravemir.lightvalue.naming;

import org.kravemir.lightvalue.model.PropertyDef;

public interface NameProvider {
    String getAccessorName(PropertyDef propertyDef);

    String getSingularAccessorName(PropertyDef propertyDef);

    String getVariableName(PropertyDef propertyDef);
}
