package org.kravemir.lightvalue.naming;

public class Pluralizer {

    private static final char[] CONSONANTS = new char[]{
    };

    public String pluralize(String singular) {
        if (endsWith(singular, "s", "x", "z", "ch", "sh")) {
            return singular + "es";
        }

        if (endsWith(singular, "y") && isConsonant(singular, -2)) {
            return singular.substring(0, singular.length() - 1) + "ies";
        }

        return singular + "s";
    }

    private boolean isConsonant(String singular, int position) {
        if (position < 0) {
            position += singular.length();
        }
        return isConsonant(singular.charAt(position));
    }

    private boolean isConsonant(char c) {
        switch (c) {
            case 'b':
            case 'c':
            case 'd':
            case 'f':
            case 'g':
            case 'h':
            case 'j':
            case 'k':
            case 'l':
            case 'm':
            case 'n':
            case 'p':
            case 'q':
            case 'r':
            case 's':
            case 't':
            case 'v':
            case 'w':
            case 'x':
            case 'z':
                return true;
            default:
                return false;
        }
    }

    private boolean endsWith(String singular, String... postfixes) {
        for (String postfix : postfixes) {
            if (singular.endsWith(postfix)) {
                return true;
            }
        }
        return false;
    }
}
