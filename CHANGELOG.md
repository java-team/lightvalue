# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.1]
### Changed
- Fix: generate valid equals for empty model class
- Fix: make JacksonMixIns accessible from different package

## 0.8.0
### Added
- TODO

[0.8.1]: https://gitlab.com/kravemir/lightvalue/compare/0.8.0...0.8.1
